## Table of contents
* [Overview](#overview)
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage Examples](#examples)
* [To Do](#to-do)

---

## Overview
This is an extended version of my interview task at Revolut.
Given an input as JSON array, each element is a flat dictionary, write 
a program that will parse this JSON, return a nested dictionary of dictionaries 
of arrays, with keys specified in command line arguments, and the leaf values 
as arrays of flat dictionaries matching appropriate groups like
`python nest.py nesting_level_1 nesting_level_2 ... nesting_level_n`.

For example, when invoked like this: `cat input.json | python nest.py currency country city` 
the output should be in JSON format and look like the content of this file: **examples/output.json**.


Please note, that the nesting keys should be stripped out from the dictionaries 
in the leaves. Also, please note that the program should support an arbitrary 
number of arguments, that is arbitrary levels of nesting.

---
## Technologies
* Python 3.9
* Flask & Flask-RESTful
* Docker & Docker-Compose

---

## Setup
Type one of the following commands to run application in development, production or testing environment:

* `make run-server-dev build=true`
* `make run-server-prod build=true`
* `make run-tests`

To display all available Makefile commands, type:

* `make help`

Stop development or production server:

* `make stop-server-dev`
* `make stop-server-prod`

OpenAPI specification is available at:

* `http://localhost:8000/apidocs`

---
## Usage examples
#### JSON API
```
curl localhost:8000/api/v1/nest\
 -H "Content-Type: application/json"\
 -u user:pass\
 -d "@$PWD/examples/input_api.json"\
 -X POST
```

Feel free to edit **input_api.json** file and try it out again.

#### CLI tool
Remember to activate a virtual environment first. You can try it out with different keys combinations.

```
cat examples/input_cli.json | python nest_cli.py country currency city
```

---
## To Do
* Run NGINX as non-root
* Add integration tests
