from typing import List, Dict

import pytest

from nest.app.nest import RecursiveNest, IterativeNest
from tests.conftest import input_data_invalid, input_data_valid


class RecursiveNestTest:
    @pytest.mark.parametrize("keys, input_data, output_data", input_data_valid)
    def test_recursive_nest_returns_expected_result(
        self,
        keys: List,
        input_data: List,
        output_data: Dict
    ) -> None:
        nest = RecursiveNest(keys, input_data)
        assert nest.nest_data() == output_data

    @pytest.mark.parametrize("keys, input_data", input_data_invalid)
    def test_recursive_nest_raises_exception_when_invalid_request_data(
        self,
        keys: List,
        input_data: List,
    ) -> None:
        nest = RecursiveNest(keys, input_data)
        with pytest.raises(KeyError):
            nest.nest_data()


class IterativeNestTest:
    @pytest.mark.parametrize("keys, input_data, output_data", input_data_valid)
    def test_iterative_nest_returns_expected_result(
        self,
        keys: List,
        input_data: List,
        output_data: Dict
    ) -> None:
        nest = IterativeNest(keys, input_data)
        assert nest.nest_data() == output_data

    @pytest.mark.parametrize("keys, input_data", input_data_invalid)
    def test_iterative_nest_raises_exception_when_invalid_request_data(
        self,
        keys: List,
        input_data: List,
    ) -> None:
        nest = IterativeNest(keys, input_data)
        with pytest.raises(KeyError):
            nest.nest_data()
