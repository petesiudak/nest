import copy

input_data = [{
    "country": "US",
    "city": "Boston",
    "currency": "USD",
    "amount": 100
  }, {
    "country": "FR",
    "city": "Paris",
    "currency": "EUR",
    "amount": 20
  }, {
    "country": "FR",
    "city": "Lyon",
    "currency": "EUR",
    "amount": 11.4
  }, {
    "country": "ES",
    "city": "Madrid",
    "currency": "EUR",
    "amount": 8.9
  }, {
    "country": "UK",
    "city": "London",
    "currency": "GBP",
    "amount": 12.2
  }, {
    "country": "UK",
    "city": "London",
    "currency": "FBP",
    "amount": 10.9
  }
]

single_nest_output = {
    "US": [{
        "city": "Boston",
        "currency": "USD",
        "amount": 100
    }],
    "FR": [{
        "city": "Paris",
        "currency": "EUR",
        "amount": 20
    }, {
        "city": "Lyon",
        "currency": "EUR",
        "amount": 11.4
    }],
    "ES": [{
        "city": "Madrid",
        "currency": "EUR",
        "amount": 8.9
    }],
    "UK": [{
        "city": "London",
        "currency": "GBP",
        "amount": 12.2
    }, {
        "city": "London",
        "currency": "FBP",
        "amount": 10.9
    }]
}

double_nest_output = {
    "US": {
        "USD": [{
            "city": "Boston",
            "amount": 100
        }]
    },
    "FR": {
        "EUR": [{
            "city": "Paris",
            "amount": 20
        }, {
            "city": "Lyon",
            "amount": 11.4
        }]
    },
    "ES": {
        "EUR": [{
            "city": "Madrid",
            "amount": 8.9
        }]
    },
    "UK": {
        "GBP": [{
            "city": "London",
            "amount": 12.2
        }],
        "FBP": [{
            "city": "London",
            "amount": 10.9
        }]
    }
}

triple_nest_output = {
    "US": {
        "USD": {
            "Boston": [{"amount": 100}]
        }
    },
    "FR": {
        "EUR": {
            "Paris": [{"amount": 20}],
            "Lyon": [{"amount": 11.4}]
        }
    },
    "ES": {
        "EUR": {
            "Madrid": [{"amount": 8.9}]
        }
    },
    "UK": {
        "GBP": {
            "London": [{"amount": 12.2}]
        },
        "FBP": {
            "London": [{"amount": 10.9}]
        }
    }
}

input_data_valid = [
    ([], input_data, input_data),
    (["country"], input_data, single_nest_output),
    (["country", "currency"], input_data, double_nest_output),
    (["country", "currency", "city"], input_data, triple_nest_output),
]

invalid_data = copy.deepcopy(input_data)
del invalid_data[0]["country"]

input_data_invalid = [
    (["invalid"], input_data),
    (["country", "invalid"], input_data),
    (["country"], invalid_data)
]
