import os
from distutils.util import strtobool

ENV = os.environ.get("ENV", default="production")
DEBUG = strtobool(os.environ.get("DEBUG", default="False"))
SECRET_KEY = os.environ.get("SECRET_KEY", default=os.urandom(16))
