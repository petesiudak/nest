import logging

from dotenv import load_dotenv
from flasgger import Swagger
from flask import Flask
from flask_restful import Api

from app.api.nest_api import NestAPI

load_dotenv()


def create_app() -> Flask:
    gunicorn_logger = logging.getLogger('gunicorn.error')

    app = Flask(__name__)
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    app.config.from_pyfile("config.py")
    app.config["SWAGGER"] = {
        "openapi": "3.0.1",
        "uiversion": 3
    }

    Swagger(app=app, template={
        "info": {
            "title": "Nest API documentation",
            "version": "0.1.1",
            "contact": {
                "name": "Piotr Siudak",
                "email": "siudakp@gmail.com",
            }
        }
    })

    api = Api(app, prefix="/api/v1/")
    api.add_resource(NestAPI, "nest")

    return app
