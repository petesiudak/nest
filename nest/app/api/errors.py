from datetime import datetime
from typing import Dict


def build_error_message(
    exception: Exception,
    error_code: int,
    date_time: datetime
) -> Dict:
    return {
        "error": {
            "code": error_code,
            "message": "Invalid request data",
            "details": f"Please verify keys along with data. {exception}. "
                       f"API specification is available at /apidocs",
            "timestamp": date_time
        }
    }
