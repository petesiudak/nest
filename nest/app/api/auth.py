import os

from flask_httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()
user_credentials = {os.environ.get("USER"): os.environ.get("PASSWORD")}


@auth.verify_password
def verify(username: str, password: str) -> bool:
    if not (username and password):
        return False
    return user_credentials.get(username) == password
