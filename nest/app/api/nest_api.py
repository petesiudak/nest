import datetime

from flasgger import swag_from
from flask import request, Response, json, current_app
from flask_restful import Resource

from app.api.auth import auth
from app.api.errors import build_error_message
from app.nest import IterativeNest


class NestAPI(Resource):
    mime_type = "application/json"
    error_code = 400

    @auth.login_required
    @swag_from("spec.yml", validation=True)
    def post(self) -> Response:
        request_data = request.get_json()
        try:
            nest = IterativeNest(
                request_data["keys"],
                request_data["data"]
            )
            nested_data = nest.nest_data()
        except KeyError as error:
            current_app.logger.error(f'Invalid payload: {error}')
            err_msg = build_error_message(
                error,
                self.error_code,
                datetime.datetime.now()
            )
            response = Response(
                json.dumps(err_msg),
                mimetype=self.mime_type,
                status=self.error_code
            )
        else:
            response = Response(
                json.dumps(nested_data),
                mimetype=self.mime_type
            )

        return response
