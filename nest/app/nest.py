import copy
from typing import List, Union, Dict


class RecursiveNest:
    def __init__(self, keys: List, data: Union[List, Dict]) -> None:
        self.keys = copy.copy(keys)
        self.data = copy.deepcopy(data)

    def nest_data(self) -> Union[Dict, List]:
        """
        A recursive implementation of a nesting algorithm representing
        divide and conquer approach. Input data is divided into smaller
        parts along with a consecutive nest level and a given nest key.

        :return: A nested dictionary of dictionaries of arrays, with keys
        specified and the leaf values as arrays of flat dictionaries matching
        appropriate groups.
        """
        if not self.keys:
            return self.data

        result = {}
        key = self.keys.pop(0)

        if isinstance(self.data, dict):
            for key, value in self.data.items():
                nest = RecursiveNest([key], value)
                result[key] = nest.nest_data()

        if isinstance(self.data, list):
            for element in self.data:
                try:
                    value = element.pop(key)
                except KeyError:
                    raise KeyError(
                        f"Error with a nest key '{key}' "
                        f"and payload '{element}'"
                    )
                if value not in result:
                    result[value] = []
                result[value].append(element)

        nest = RecursiveNest(self.keys, result)
        nested_data = nest.nest_data()

        return nested_data


class IterativeNest:
    def __init__(self, keys: List, data: List) -> None:
        self.keys = copy.copy(keys)
        self.data = copy.deepcopy(data)

    def nest_data(self) -> Union[Dict, List]:
        """
        An iterative implementation of a nesting algorithm in which a reference
        to a nested dictionary is swapped along with a consecutive nest level
        and a nest key.

        :return: A nested dictionary of dictionaries of arrays, with keys
        specified and the leaf values as arrays of flat dictionaries matching
        appropriate groups.
        """
        if len(self.keys) == 0:
            return self.data

        last_key = self.keys.pop()
        result = {}
        temp = result

        for element in self.data:
            for key in self.keys:
                try:
                    value = element.pop(key)
                except KeyError:
                    raise KeyError(
                        f"Error with a nest key '{key}' "
                        f"and payload '{element}'"
                    )

                if value not in temp:
                    temp[value] = {}
                temp = temp[value]

            value = element.pop(last_key)
            if value not in temp:
                temp[value] = [element]
            else:
                temp[value].append(element)

            temp = result

        return result
