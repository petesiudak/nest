FROM python:3.9.1-alpine as base
MAINTAINER Piotr Siudak "siudakp@gmail.com"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

WORKDIR /usr/src/nest/nest/app
COPY nest/app .
RUN pip install --upgrade pip --no-cache-dir -q -r requirements/base.txt

FROM base as test

RUN pip install --upgrade pip --no-cache-dir -q -r requirements/test.txt
WORKDIR /usr/src/nest/tests
COPY tests .
COPY pytest.ini /usr/src/nest

FROM base AS prod

ARG PROJECT_DIR=/home/user/nest/nest/app
WORKDIR $PROJECT_DIR

COPY --from=base /usr/src/nest/nest/app .
ARG USER=user

RUN addgroup -S $USER && adduser -S $USER -G $USER
RUN chown -R $USER:$USER $PROJECT_DIR
RUN mkdir /home/user/nest/gunicorn
RUN chown -R $USER:$USER /home/user/nest/gunicorn
RUN pip install --upgrade --no-cache-dir -q -r requirements/prod.txt

USER $USER
