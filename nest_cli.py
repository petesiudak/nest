import argparse
import json
import sys
from json import JSONDecodeError

from nest.app.nest import RecursiveNest


def cli():
    parser = argparse.ArgumentParser(
        description="Data to nest based on specified keys"
    )
    parser.add_argument(
        "keys",
        type=str,
        nargs="*",
        help="Keys based on which data is nested"
    )
    args = parser.parse_args()

    try:
        input_data = json.load(sys.stdin)
    except JSONDecodeError as error:
        sys.stderr.write(str(error))
        sys.exit()

    nest = RecursiveNest(args.keys, input_data)

    try:
        sys.stdout.write(json.dumps(nest.nest_data()))
    except KeyError as error:
        sys.stderr.write(
            f"Invalid payload, please verify "
            f"keys along with data. ***{error}***"
        )
        sys.exit()


if __name__ == "__main__":
    cli()
