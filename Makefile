PYTHON_VERSION = python3.9
APP_NAME = nest
DOCKER_FILE_PROD = -f docker-compose.prod.yml
DOCKER_FILE_TEST = -f docker-compose.test.yml
DOCKER_FILE_DEV = -f docker-compose.yml

.DEFAULT: help
help:
	@echo "make run-server-prod"
	@echo "      Runs all defined prod services. To build images add 'build=true' argument"
	@echo "make stop-server-prod"
	@echo "      Brings all defined prod services down and removes containers"
	@echo "make run-server-dev"
	@echo "      Runs all defined dev services. To build images add 'build=true' argument"
	@echo "make stop-server-dev"
	@echo "      Brings all defined dev services down and removes containers"
	@echo "make run-tests"
	@echo "      Runs tests in a container"
	@echo "make remove-volumes"
	@echo "      Removes all defined data volumes"

run-server-prod:
ifeq ($(build), true)
	@docker-compose $(DOCKER_FILE_PROD) build --no-cache
endif
	@test -f .env || cp envs/.env.example .env
	@docker-compose $(DOCKER_FILE_PROD) up -d

stop-server-prod:
	@docker-compose $(DOCKER_FILE_PROD) down

run-server-dev:
ifeq ($(build), true)
	@docker-compose $(DOCKER_FILE_DEV) build --no-cache
endif
	@docker-compose $(DOCKER_FILE_DEV) up -d

stop-server-dev:
	@docker-compose $(DOCKER_FILE_DEV) down

run-tests:
	@docker-compose $(DOCKER_FILE_TEST) --env-file .env.test. up --build
	@docker-compose $(DOCKER_FILE_TEST) rm -fs

remove-volumes:
	@docker volume rm nginx gunicorn
